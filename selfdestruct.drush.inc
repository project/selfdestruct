<?php // $Id$

/**
 * @file Self Destruct drush command.
 */
 
/**
 * Implementation of hook_drush_command()
 */
function selfdestruct_drush_command() {
  $items = array();
  
  $items['selfdestruct'] = array(
    'callback' => 'selfdestruct_drush_selfdestruct',
    'description' => 'Drop all tables from the site database. USE WITH CAUTION.',
  );
  
  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function selfdestruct_drush_help($section) {
  switch ($section) {
    case 'drush:selfdestruct':
      return dt('Drops all tables from the database. Primarily useful during the development of Installation Profiles.');
  }
}

/**
 * Callback function
 */
function selfdestruct_drush_selfdestruct() {
  if (drush_confirm(dt("Are you sure you want to reset the site's database? All data will be completely lost."))) {
    selfdestruct_bigredbutton();
    drush_print(dt("Dropped database tables."));
  }
}